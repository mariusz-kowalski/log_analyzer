# frozen_string_literal: true

class Stats
  include Enumerable

  def initialize(&initiate_object)
    @initiate_object = initiate_object
    @stats = {}
  end

  def [](key)
    @stats[key] ||= @initiate_object.call
  end

  def []=(key, value)
    @stats[key] = value
  end

  def each
    if block_given?
      @stats.each { |*args| yield(*args) }
    else
      @stats.each
    end
  end
end
