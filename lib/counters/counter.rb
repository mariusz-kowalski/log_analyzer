# frozen_string_literal: true

require_relative '../stats.rb'

module Counters
  class Counter
    attr_reader :stats

    def initialize
      @stats = Stats.new(&initiate_stat_object)
    end

    def to_proc
      proc { |entry| call(entry) }
    end

    def self.[](initiate_stat_object)
      clone.tap do |klass|
        klass.define_method :initiate_stat_object do
          initiate_stat_object
        end
      end
    end

    def initiate_stat_object
      -> { 0 }
    end
  end
end
