# frozen_string_literal: true

require_relative 'counter.rb'

module Counters
  class Visits < Counter
    def call(entry)
      stats[entry[:page]] += 1
    end
  end
end
