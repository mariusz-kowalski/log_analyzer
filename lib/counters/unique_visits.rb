# frozen_string_literal: true

require 'set'

require_relative 'counter.rb'

module Counters
  class UniqueVisits < Counter[-> { Set.new }]
    def call(entry)
      @stats[entry[:page]] << entry[:ip]
    end

    def stats
      @stats.map { |key, value| [key, value.count] }
            .to_h
            .tap { |result| result.default = 0 }
    end
  end
end
