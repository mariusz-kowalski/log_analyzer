# frozen_string_literal: true

class PrintStats
  def initialize(stats, order: nil, unique: false)
    @stats = stats
    @order = order
    @unique = unique
  end

  def call(output = STDOUT)
    @stats.sort_by(&sorter)
          .each(&printer(output))
  end

  private

  def sorter
    if @order == 'asc'
      proc { |_page, count| count }
    else
      proc { |_page, count| -count }
    end
  end

  def printer(target)
    if @unique
      proc { |page, count| target.puts "#{page} #{count} unique views" }
    else
      proc { |page, count| target.puts "#{page} #{count} visits" }
    end
  end
end
