# frozen_string_literal: true

class LogStat
  def initialize(file, &mapper)
    file.each_line do |line|
      mapper.call(parse(line))
    end
  end

  private

  def parse(line)
    page, ip = line.split
    { page: page, ip: ip }
  end
end
