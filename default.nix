with (import <nixpkgs> {});

let
  rubyenv = bundlerEnv {
    name = "rb";
    inherit ruby;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
  };
in
stdenv.mkDerivation {
  name = "LogApp";
  version = "0.0.1";

  buildInputs = [
    stdenv

    zlib
    openssl
    gdbm
    ncurses
    readline
    groff
    libyaml
    libffi
    libiconv
    libunwind

    ruby
    bundler

    bundix

    rubyenv
  ];
}
