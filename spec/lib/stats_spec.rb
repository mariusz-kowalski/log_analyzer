# frozen_string_literal: true

require 'spec_helper.rb'

require_relative '../../lib/stats.rb'

RSpec.describe Stats do
  describe '#[]' do
    subject { described_class.new { Set.new } }
    it 'initiate each entry with initiate_object' do
      subject[:some_stat] << :a
      subject[:some_stat] << :b
      subject[:other_stat] << :c
      expect(subject[:some_stat]).to eq Set[:a, :b]
      expect(subject[:other_stat]).to eq Set[:c]
    end
  end

  describe '#each' do
    subject do
      described_class.new { 0 }.tap do |stats|
        stats[:a] += 1
        stats[:a] += 1
        stats[:b] += 1
      end
    end

    it 'iterates on every entry' do
      entries = Set[]
      subject.each { |key, _| entries << key }
      expect(entries).to eq Set[:a, :b]
    end
  end
end
