# frozen_string_literal: true

require 'spec_helper.rb'

require_relative '../../../lib/counters/unique_visits.rb'

RSpec.describe Counters::UniqueVisits do
  let(:page) { '/' }
  let(:entry) { { page: page, ip: '1.1.1.1' } }
  let(:entry2) { { page: page, ip: '2.2.2.2' } }
  let(:counter) { described_class.new }

  describe '#call' do
    context 'with single visit per page' do
      subject do
        counter.call(entry)
      end

      it 'increases entry by 1' do
        expect { subject }
          .to change { counter.stats[page] }
          .from(0)
          .to(1)
      end
    end

    context 'with multiple visit per page' do
      subject do
        counter.call(entry)
        counter.call(entry)
        counter.call(entry2)
      end

      it 'count only unique visits' do
        expect { subject }
          .to change { counter.stats[page] }
          .from(0)
          .to(2)
      end
    end
  end
end
