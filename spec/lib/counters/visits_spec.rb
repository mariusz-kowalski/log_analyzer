# frozen_string_literal: true

require 'spec_helper.rb'

require_relative '../../../lib/counters/visits.rb'

RSpec.describe Counters::Visits do
  let(:page) { '/' }
  let(:entry) { { page: page, ip: '1.1.1.1' } }
  let(:counter) { described_class.new }

  describe '#call' do
    it 'increases entry by 1' do
      expect { counter.call(entry) }
        .to change { counter.stats[page] }
        .from(0)
        .to(1)
    end
  end
end
