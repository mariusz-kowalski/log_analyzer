# frozen_string_literal: true

require 'spec_helper.rb'

require_relative '../../lib/print_stats.rb'

RSpec.describe PrintStats do
  let(:stats) do
    {
      '/home' => 2,
      '/about' => 3
    }
  end
  let(:output) { StringIO.new }

  describe '#call' do
    context 'not unique stats' do
      subject { described_class.new(stats).call(output) }

      context 'with no order given' do
        it 'prints stats in desc order' do
          expect { subject }
            .to change { output.string }
            .to "/about 3 visits\n" \
                "/home 2 visits\n"
        end
      end

      context 'with order asc' do
        subject { described_class.new(stats, order: 'asc').call(output) }

        it 'prints stats in asc order' do
          expect { subject }
            .to change { output.string }
            .to "/home 2 visits\n" \
                "/about 3 visits\n"
        end
      end
    end

    context 'unique visits stats' do
      subject { described_class.new(stats, unique: true).call(output) }

      it 'prints unique stats in desc order' do
          expect { subject }
            .to change { output.string }
            .to "/about 3 unique views\n" \
                "/home 2 unique views\n"
        end
    end
  end
end
