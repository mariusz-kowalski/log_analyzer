#!/usr/bin/env ruby
# frozen_string_literal: true

require 'optparse'

require_relative 'lib/log_stat.rb'
require_relative 'lib/counters/visits.rb'
require_relative 'lib/counters/unique_visits.rb'
require_relative 'lib/print_stats.rb'

log_file = ARGV.last

options = {
  order: 'desc'
}

OptionParser.new do |opts|
  opts.banner = 'Usage: app.rb [options] LOG_FILE'

  opts.on('-u', '--unique', 'Count unique visits') do
    options[:unique] = true
  end

  opts.on('-o', '--order ORDER', %w[asc desc], 'Sort order', 'asc, desc') do |value|
    options[:order] = value
  end
end.parse!

counter = (options[:unique] ? Counters::UniqueVisits : Counters::Visits).new

File.open(log_file) do |file|
  LogStat.new(file, &counter)
end

PrintStats.new(
  counter.stats,
  **options
).call
