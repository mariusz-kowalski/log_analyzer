# Log Analyzer

App that prints stats from given log file.

## Installation

Clone repository and run:

```sh
bundle install
```

## Run

In project directory.

```sh
bundle exec ./app webserver.log
```

## Help

```sh
bundle exec ./app --help
```
